async function fetchAndDisplay() {
    const option = document.getElementById('options').value;
    const cocktails = await fetchCocktails(option);
    displayCocktails(cocktails);
    displayMessage(cocktails.length, option);
}

async function fetchCocktails(type) {
    const response = await fetch(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${type}`);
    const data = await response.json();
    return data.drinks;
}

function displayCocktails(cocktails) {
    const cocktailsContainer = document.getElementById('cocktails');
    cocktailsContainer.innerHTML = '';
    
    cocktails.forEach(cocktail => {
        const cocktailDiv = document.createElement('div');
        cocktailDiv.classList.add('cocktail');
        
        const cocktailName = document.createElement('p');
        cocktailName.textContent = cocktail.strDrink;
        
        const cocktailImg = document.createElement('img');
        cocktailImg.src = cocktail.strDrinkThumb;
        cocktailImg.alt = cocktail.strDrink;

        cocktailDiv.appendChild(cocktailImg);
        cocktailDiv.appendChild(cocktailName);

        cocktailsContainer.appendChild(cocktailDiv);
    });
}

function displayMessage(total, option) {
    const messageContainer = document.getElementById('message');
    messageContainer.textContent = `Total de cócteles ${option === 'Non_Alcoholic' ? 'sin' : 'con'} alcohol: ${total}`;
}

function clearCocktails() {
    document.getElementById('cocktails').innerHTML = '';
    document.getElementById('message').textContent = '';
}